﻿using System;

namespace Polymorphism
{
   public class Student
    {
        void read(string name)
        {
            Console.WriteLine("Name:{0}",name);
        }
        void read(int age)
        {
            Console.WriteLine("Age:{0}", age);
        }
         static void Main(string[] args)
        {
            Student s = new Student();
            s.read("Ram");
            s.read(25);
            Console.ReadKey();
        }
    }
}
